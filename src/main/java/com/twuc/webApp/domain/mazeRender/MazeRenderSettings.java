package com.twuc.webApp.domain.mazeRender;

/**
 * 代表了整个迷宫的渲染参数。
 */
public class MazeRenderSettings {
    private int cellSize;
    private int margin;

    /**
     * 创建一个 {@link MazeRenderSettings} 实例。
     *
     * @param cellSize 迷宫的每一个渲染节点的默认像素大小。
     * @param margin 迷宫距离图像边界的像素距离。
     */
    public MazeRenderSettings(int cellSize, int margin) {
        this.cellSize = cellSize;
        this.margin = margin;
    }

    /**
     * 获得迷宫的每一个渲染节点的默认像素大小。该大小可用于计算迷宫总大小以及用于计算每一个节点的实际大小和位置。
     *
     * @return 迷宫的每一个渲染节点的默认像素大小。
     */
    public int getCellSize() {
        return cellSize;
    }

    /**
     * 获得迷宫距离图像边界的像素距离。
     *
     * @return 迷宫距离图像边界的距离。
     */
    public int getMargin() {
        return margin;
    }
}
